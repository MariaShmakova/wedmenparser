<?php

$start = time();

PageAnalyzer::$basedir = __DIR__;

while ((time() - $start) < 120 && $page = PageAnalyzer::getPage()){
    PageAnalyzer::load($page);
}

