<?php

class PageAnalyzer
{
    protected static $lastPage = 1673;
    protected static $pathToFinishedPage = 'finishedPage.txt';
    protected static $output = 'output.csv';
    protected static $siteURL = 'https://mywed.com';
    public static $basedir;

    public static function getPage()
    {
        $finishedPages = self::getFinishedPages();
        $page = array_rand(array_diff(range(0, self::$lastPage), $finishedPages));
        return $page;
    }

    private static function getFinishedPages()
    {
        $arPages = file(self::$basedir . '/' . self::$pathToFinishedPage);

        return array_map(function ($value) {
            return (integer)$value;
        }, $arPages);
    }

    public static function setPageFinished($page)
    {
        return is_numeric($page) ? file_put_contents(self::$basedir . '/' .self::$pathToFinishedPage, $page . PHP_EOL, FILE_APPEND) : false;
    }

    public static function load($number)
    {
	echo "Loading page {$number}\n";

	self::setPageFinished($number);
        $page = self::getCurlResult(self::getPageUrl($number));

        $crawler = new \Symfony\Component\DomCrawler\Crawler($page);
        $items = $crawler->filter('.title-author.wide a');
        $links = array();
        foreach ($items as $item) {
            /* @var $item DOMElement */
            $links[] = $item->getAttribute('href');
        }
        $links = array_unique($links);

        $datas = self::getDataOnLinks($links);

        self::writeDatasInCsv($datas);

	echo "Page done\n";
    }

    private static function getDataOnLinks($links)
    {
        $result = array();

        foreach ($links as $key => $link) {
		
	    echo "Loading page";

            $page = self::getCurlResult(self::$siteURL . $link);
            $crawler = new \Symfony\Component\DomCrawler\Crawler($page);

	    $result[$key]['url'] = self::$siteURL . $link;

            $name = $crawler->filter('section .cap-author-name');
            $result[$key]['name'] = (!empty($name) && $name->count()) ? trim($name->text()) : NULL;

            $country = $crawler->filter('meta[itemprop="addressCountry"]');
            $result[$key]['country'] = (!empty($country) && $country->count()) ? $country->attr("content") : NULL;

            $city = $crawler->filter('meta[itemprop="addressLocality"]');
            $result[$key]['city'] = (!empty($city) && $city->count()) ? $city->attr("content") : NULL;

            $phone = $crawler->filter('span[itemprop="telephone"] a');
            $result[$key]['phone'] = (!empty($phone) && $phone->count()) ? trim($phone->text()) : NULL;

            $site = $crawler->filter('.profile-user-contacts a.link-def');
            $result[$key]['site'] = (!empty($site) && $site->count()) ? trim($site->text()) : NULL;

//            echo "<pre>" . var_dump($result) . "</pre>";
//            die();

	    echo " {$result[$key]['name']}\n";
        }

        return $result;
    }

    private static function getCurlResult($url)
    {
        $timeSleep = array_rand(range(1, 5));
        sleep($timeSleep);
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 10,
            CURLOPT_URL => $url
        ));
        $page = curl_exec($ch);
        curl_close($ch);
        return $page;
    }

    private static function getPageUrl($number)
    {
        $url = self::$siteURL . "/ru/photographers";
        return ($number > 0) ? $url . "/p" . $number : $url;
    }


    private static function writeDatasInCsv($datas)
    {
        $handle = fopen(self::$basedir . '/' .self::$output, "a");
        foreach ($datas as $fields){
            fputcsv($handle, $fields, ";");
        }
        fclose($handle);
    }

}
